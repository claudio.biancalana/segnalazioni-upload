FROM node:12-alpine
MAINTAINER Claudio Biancalana - ANAC <c.biancalana@anticorruzione.it>

COPY ./ /src
WORKDIR /src

RUN npm cache clean --force && \
    npm install
    
RUN chgrp -R 0 /src && \
    chmod -R g=u /src

EXPOSE 8080

ENTRYPOINT  ["node", "index"]
